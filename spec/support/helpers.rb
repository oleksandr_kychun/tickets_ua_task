require 'support/helpers/user_helpers'
require 'support/helpers/conversation_helpers'

RSpec.configure do |config|
  config.include Features::UserHelpers
  config.include Features::ConversationHelpers
end
