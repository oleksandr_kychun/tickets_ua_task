RSpec.shared_context "unread data setup", :shared_context => :metadata do
  let(:user) { create_valid_user['model'] }
  let(:receiver) { create_valid_user(email: 'rec@gmail.com')['model'] }
  let(:other_receiver) { create_valid_user(email: 'rec2@gmail.com')['model'] }

  before(:example) do
    conv1 = create_conversation(receiver, user)['model']
    conv1.messages.create(body: 'Hey', user_id: receiver.id)
    conv1.messages.create(body: 'Hey', user_id: user.id)
    conv1.messages.create(body: 'Hey', user_id: receiver.id)

    conv2 = create_conversation(other_receiver, user)['model']
    conv2.messages.create(body: 'Hey', user_id: receiver.id)
    conv2.messages.create(body: 'Hey', user_id: user.id)
  end
end
