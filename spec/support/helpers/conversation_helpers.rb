module Features
  module ConversationHelpers
    def create_conversation(receiver, sender)
      Conversation::Operation::Create.call({ receiver_id: receiver.id },
                                           'current_user' => sender)
    end
  end
end
