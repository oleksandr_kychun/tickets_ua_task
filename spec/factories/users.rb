FactoryGirl.define do
  factory :user do
    email "ordinary@gmail.com"
    role 0
    factory :admin do
      email 'admin@gmail.com'
      role 2
    end
  end
end
