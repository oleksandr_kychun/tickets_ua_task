FactoryGirl.define do
  factory :message do
    body "Hey, there"
    conversation_id nil
    user_id nil
  end
end
