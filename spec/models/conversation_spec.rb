RSpec.describe Conversation do
  it { is_expected.to have_db_column(:id) }
  it { is_expected.to have_db_column(:created_at) }
  it { is_expected.to have_and_belong_to_many(:users) }
  it { is_expected.to have_many(:messages).dependent(:destroy) }
end
