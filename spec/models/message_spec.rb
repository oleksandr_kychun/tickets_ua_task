RSpec.describe Message do
  it { is_expected.to have_db_column(:body) }
  it { is_expected.to define_enum_for(:status).with(%i[unread seen]) }
  it { is_expected.to belong_to(:user) }
  it { is_expected.to belong_to(:conversation) }
end
