RSpec.describe NoUser do
  it { is_expected.to respond_to(:whatever?) }
  it { is_expected.to respond_to(:id) }
  it do
    is_expected.to receive(:anything?).and_return(false)
    subject.anything?
  end
end
