require 'rails_helper'

RSpec.describe User, type: :model do
  it { is_expected.to define_enum_for(:role).with(%i[user vip admin]) }
  it { is_expected.to have_db_column(:auth_meta_data) }
  it { is_expected.to have_db_column(:email) }
  it { is_expected.to have_db_column(:first_name) }
  it { is_expected.to have_db_column(:last_name) }
  it { is_expected.to have_db_column(:role) }
  it { is_expected.to have_and_belong_to_many(:conversations) }
end
