RSpec.describe 'Unread Data', type: :feature do
  include_context 'unread data setup'

  before(:example) { browser_login }

  scenario 'User is able to see unread data when logins' do
    visit root_path
    expect(page).to have_content('rec@gmail.com 2 unread messages')
    expect(page).to have_content('rec2@gmail.com 1 unread messages')
  end
end
