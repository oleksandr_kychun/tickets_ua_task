RSpec.describe 'UserIntegration', type: :feature do
  context 'ordinary user' do
    context 'own profile' do
      before(:example) do
        @op = create_valid_user
        browser_login
      end

      let(:user) { @op['model'] }

      scenario 'User is able to update profile' do
        visit edit_user_path(user)
        within '#edit_user' do
          fill_in('First name', with: update_attributes[:first_name])
          fill_in('Last name', with: update_attributes[:last_name])
          fill_in('Password', with: update_attributes[:password])
          fill_in('Confirm password',
                  with: update_attributes[:confirm_password])
          fill_in('Current password',
                  with: update_attributes[:current_password])
          click_button 'Update profile'
        end
        user.reload

        expect(user.first_name).to eq(update_attributes[:first_name])
        expect(user.last_name).to eq(update_attributes[:last_name])
      end

      scenario 'User can\'t create user' do
        visit new_user_path
        expect(page).to have_current_path(root_path)
      end
    end

    context 'other profile' do
      before(:example) do
        create_valid_user
        browser_login
        @stranger = create_valid_user(email: 'stranger@gmail.com',
                                      password: '12345678')
      end

      let(:user) { @stranger['model'] }

      scenario 'User can\'t update profile' do
        visit edit_user_path(user)

        expect(page).to have_current_path(root_path)
      end
    end
  end

  context 'admin user' do
    before(:example) do
      @admin = create_admin
      browser_login(email: @admin.email, password: 'complicated_stuf')
    end

    scenario 'Admin is able to create user' do
      visit new_user_path
      expect {
        within '#new_user' do
          fill_in('Email', with: user_attributes[:email])
          fill_in('First name', with: user_attributes[:first_name])
          fill_in('Last name', with: user_attributes[:last_name])
          fill_in('Password', with: user_attributes[:password])
          click_button('Create user')
        end
      }.to change { User.count }.by 1
    end

    scenario 'Admin can\'t create user without email and password' do
      visit new_user_path
      within '#new_user' do
        fill_in('First name', with: user_attributes[:first_name])
        fill_in('Last name', with: user_attributes[:last_name])
        click_button('Create user')
      end
      expect(page).to have_content('can\'t be blank')
    end
  end

  def user_attributes
    {
      email: 'manualy_created@gmail.com',
      first_name: 'Paul',
      last_name: 'Williams',
      password: 'easy_breasy'
    }
  end
end
