RSpec.describe 'User index page', type: :feature do
  context 'ordinary user' do
    let!(:user) { create_valid_user['model'] }
    before(:example) do
      browser_login
      create_valid_user(email: 'dummy1@gmail.com')
      create_valid_user(email: 'dummy2@gmail.com')
      create_admin
    end

    scenario 'User is able to see other users in system except admin' do
      visit root_path
      expect(page).to have_content('dummy1@gmail.com')
      expect(page).to have_link('conversation')
    end

    scenario 'User is able to go to conversation page with given user',
             js: true do
      visit root_path
      find('a', text: 'conversation with dummy1@gmail.com').click
      expect(page).to have_content('Messages history')
    end

    scenario 'User is able to go to conversation that already exists',
             js: true do
      visit root_path
      visit_conversation
      visit root_path
      visit_conversation
      expect(page).to have_content('Messages history')
      expect(page).to have_link('Back')
    end

    scenario 'User is able to write a message' do
      visit root_path
      visit_conversation
      within('#new_message') do
        fill_in('message_body', with: message)
        click_button 'Send'
      end
      expect(page).to have_content(message)
    end
  end

  def visit_conversation
    find('a', text: 'conversation with dummy1@gmail.com').click
  end

  def message
    'Hey, what\'s new?'
  end
end
