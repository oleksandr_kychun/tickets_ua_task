RSpec.describe 'Unread mesages grouped' do
  include_context 'unread data setup'

  before(:example) { browser_login }

  scenario 'User is able to preview unread messages' do
    visit root_path

    find('.alert.alert-warning').click_link
    expect(page).to have_content('Hey')
  end

  scenario 'When user gets to the conversation unread become seen' do
    visit root_path

    find('.alert.alert-warning').click_link
    find('//div.row.col-md-offset-3.col-md-6/div[1]').click_link
    click_link 'Back'
    expect(page).not_to have_content('rec@gmail.com 2 unread messages')
  end
end
