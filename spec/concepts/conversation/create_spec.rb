RSpec.describe Conversation::Operation::Create do
  let(:sender) { create_valid_user(email: 'sender@gmail.com')['model'] }
  let(:receiver) { create_valid_user(email: 'receiver@gmail.com')['model'] }
  context 'valid data' do
    let(:operation) do
      Conversation::Operation::Create.call({ receiver_id: receiver.id },
                                           'current_user' => sender)
    end

    it 'will create conversation' do
      expect { operation }.to change { Conversation.count }
        .from(0).to(1)
    end

    it 'will create two records in join table with user' do
      expect(operation['model'].users).to eq([sender, receiver])
    end
  end

  context 'missing data' do
    let(:operation) do
      Conversation::Operation::Create.call({})
    end

    it 'would be failure' do
      expect(operation).to be_failure
    end
  end
end
