RSpec.describe ConversationPolicy do
  let(:user) { User.new(id: 5) }

  context 'valid data' do
    let(:conversation) do
      double('conversation', user_ids: [5])
    end
    subject { ConversationPolicy.new(user, conversation) }

    it { is_expected.to be_available }
    it { is_expected.to be_create }
  end

  context 'invalid data' do
    let(:conversation) do
      double('conversation', user_ids: [])
    end
    subject { ConversationPolicy.new(user, conversation) }

    it { is_expected.not_to be_available }
    it { is_expected.to be_create }
  end
end
