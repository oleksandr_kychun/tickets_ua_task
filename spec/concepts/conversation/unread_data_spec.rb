RSpec.describe Conversation::Operation::UnreadData do
  include_context 'unread data setup'
  
  context 'valid data' do
    let(:operation) do
      Conversation::Operation::UnreadData.call({},
                                               'current_user' => user)
    end

    it 'should return collection of unread messages' do
      expect(operation).to be_success
      collection1, collection2 = operation['model'].map(&:messages)
      expect(collection1.size).to eq(2)
      expect(collection2.size).to eq(1)
    end
  end
end
