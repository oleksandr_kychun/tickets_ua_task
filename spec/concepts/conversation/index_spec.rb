RSpec.describe Conversation::Operation::Index do
  include_context 'unread data setup'

  context 'valid data' do
    let(:operation) do
      Conversation::Operation::Index.call({}, 'current_user' => user)
    end

    it 'returns data about unread messages grouped by conversations' do
      expect(operation).to be_success
      expect(operation['model'].size).to eq(2)
      expect(operation['model'].first.messages.size).to eq(2)
      expect(operation['model'].last.messages.size).to eq(1)
    end
  end
end
