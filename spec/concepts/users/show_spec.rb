RSpec.describe User::Operation::Show do
  context 'valid data given' do
    let(:session_owner) { create_valid_user['model'] }
    let(:system_user) { create_valid_user(email: 'sys@gmail.com')['model'] }
    let(:operation) do
      User::Operation::Show.call(attributes,
                                 'current_user' => session_owner)
    end

    it 'finds and returns user' do
      expect(operation).to be_success
      expect(operation['model']).to eq(system_user)
    end
  end

  context 'missing data' do
    let(:operation) { User::Operation::Show.call({}) }

    it 'returns failure result' do
      expect(operation).to be_failure
    end
  end

  def attributes
    { id: system_user.id }
  end
end
