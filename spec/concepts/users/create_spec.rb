RSpec.describe User::Operation::Create do
  let(:admin) { User.new(role: 2) }
  context 'valid input' do
    let(:operation) do
      User::Operation::Create.call(user_attributes,
                                   'current_user' => admin)
    end
    it 'creates new user with provided attributes' do
      expect { operation }.to change { User.count }.by 1
      expect(operation).to be_success
      expect(operation['model'].email).to eq(user_attributes[:email])
      expect(operation['model'].first_name).to eq(user_attributes[:first_name])
      expect(operation['model'].last_name).to eq(user_attributes[:last_name])
    end
  end

  context 'empty input' do
    let(:operation) do
      User::Operation::Create.call({},
                                   'current_user' => admin)
    end

    it 'does not create record, but provides errors' do
      expect { operation }.not_to change { User.count }
      expect(operation).to be_failure
      expect(operation['contract.default'].errors.messages)
        .to include(:email, :password)
    end
  end

  def user_attributes
    {
      email: 'manualy_created@gmail.com',
      first_name: 'Paul',
      last_name: 'Williams',
      password: 'easy_breasy'
    }
  end
end
