RSpec.describe UserPolicy do
  context 'admin user' do
    subject { UserPolicy.new(build(:admin), build(:user)) }

    it { is_expected.to be_create }
    it { is_expected.to be_update }
    it { is_expected.to be_destroy }
  end

  context 'ordinary user' do
    context 'different ids' do
      subject { UserPolicy.new(build(:user, id: 8), build(:user, id: 5)) }

      it { is_expected.not_to be_create }
      it { is_expected.not_to be_update }
      it { is_expected.not_to be_destroy }
    end

    context 'the same record' do
      let(:user) { build(:user, id: 5) }
      subject { UserPolicy.new(user, user) }

      it { is_expected.not_to be_create }
      it { is_expected.to be_update }
      it { is_expected.not_to be_destroy }
    end
  end
end
