RSpec.describe User::Operation::Index do
  context 'user logged in' do
    let(:user) { create_valid_user['model'] }
    let(:operation) do
      User::Operation::Index.call({}, 'current_user' => user)
    end
    before(:example) do
      create_valid_user
      create_valid_user
    end

    it 'returns collection of users without current' do
      expect(operation).to be_success
      expect(operation['model'].count).to eq(2)
    end
  end

  context 'no current_user' do
    let(:operation) do
      User::Operation::Index.call({})
    end

    it 'returns failure' do
      expect(operation).to be_failure
    end
  end
end
