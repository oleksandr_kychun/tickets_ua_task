RSpec.describe User::Operation::Delete do
  let(:admin) { User.new(role: 2) }
  let(:ordinary_user) { User.new(role: 0) }

  before(:example) { @op = create_valid_user }
  let(:user) { @op['model'] }

  context 'valid input' do
    context 'admin' do
      let(:operation) do
        User::Operation::Delete.call({ id: user.id },
                                     'current_user' => admin)
      end
      it 'deletes user by given id' do
        expect { operation }.to change { User.count }.from(1).to(0)
      end
    end

    context 'ordinary_user' do
      let(:operation) do
        User::Operation::Delete.call({ id: user.id },
                                     'current_user' => ordinary_user)
      end
      it 'does not delete user' do
        expect { operation }.not_to change { User.count }
      end
    end
  end
end
