RSpec.describe User::Operation::Update do
  before(:example) do
    @op = create_valid_user
  end
  let(:existing_user) { User.find @op['model'].id }

  context 'valid data provided' do
    it 'changes user attributes' do
      user_attributes = update_attributes.merge(id: existing_user.id)
      expect do
        User::Operation::Update.call(user_attributes,
                                     'current_user' => existing_user)
      end
        .to change {
          existing_user.reload
          existing_user.first_name
        }.from(nil).to('Greg')
      expect(Tyrant::Authenticatable.new(existing_user)
                                    .digest?(update_attributes[:password]))
    end
  end

  context 'current_password is missing' do
    it 'does not change attributes' do
      user_attributes = {}.merge(id: existing_user.id)
      result = User::Operation::Update.call(user_attributes,
                                            'current_user' => existing_user)
      expect(result).to be_failure
      expect(result['contract.default'].errors).to include(:current_password)
    end
  end

  context 'new password provided w_o confirmation' do
    it 'does not change password' do
      user_attributes = update_attributes.merge(id: existing_user.id,
                                                confirm_password: nil)
      result = User::Operation::Update.call(user_attributes,
                                            'current_user' => existing_user)
      expect(result).to be_failure
      expect(result['contract.default'].errors).to include(:confirm_password)
    end
  end
end
