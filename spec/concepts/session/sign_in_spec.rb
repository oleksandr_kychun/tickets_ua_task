RSpec.describe Session::Operation::SignIn do
  before(:example) do
    create_valid_user
  end

  let(:user) { double('user', default_user_creds) }

  context 'valid data provided' do
    let(:operation) do
      login_valid_user
    end

    it 'expects to be successfull' do
      expect(operation).to be_success
    end
  end

  context 'invalid data provided' do
    it 'validates for emptiness' do
      operation = Session::Operation::SignIn.call({})
      errors = operation['contract.default'].errors.full_messages

      expect(operation).to be_failure
      expect(errors).not_to be_empty
      expect(errors)
        .to include('Email can\'t be blank', 'Password can\'t be blank')
    end

    it 'validates correct password' do
      operation = Session::Operation::SignIn.call(email: user.email,
                                                  password: 'random')
      errors = operation['contract.default'].errors.full_messages

      expect(operation).to be_failure
      expect(errors).not_to be_empty
      expect(errors)
        .to include('Password wrong credentials.')
    end
  end
end
