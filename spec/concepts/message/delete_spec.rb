RSpec.describe Message::Operation::Delete do
  let(:author) { create_valid_user(email: 'sender@gmail.com')['model'] }
  let!(:message) { create(:message, user_id: author.id) }

  context 'valid data' do
    let(:operation) do
      Message::Operation::Delete.call({ id: message.id },
                                      'current_user' => author)
    end

    it 'deletes message' do
      expect { operation }.to change { Message.count }
        .from(1).to(0)
    end
  end

  context 'no data provided' do
    let(:operation) do
      Message::Operation::Delete.call({})
    end

    it 'must be failure' do
      expect(operation).to be_failure
    end
  end

  context 'message does not belong to user' do
    let!(:ordinary_user) { create_valid_user['model'] }
    let(:operation) do
      Message::Operation::Delete.call({ id: message.id },
                                      'current_user' => ordinary_user)
    end

    it 'rejects not author' do
      expect(operation).to be_failure
    end
  end
end
