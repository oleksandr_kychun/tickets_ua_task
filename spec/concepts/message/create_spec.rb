RSpec.describe Message::Operation::Create do
  let!(:sender) { create_valid_user(email: 'sender@gmail.com')['model'] }
  let!(:receiver) { create_valid_user(email: 'receiver@gmail.com')['model'] }
  let!(:conversation) { create_conversation(sender, receiver)['model'] }

  context 'valid input' do
    let(:operation) do
      Message::Operation::Create.call(message_params,
                                      'current_user' => sender)
    end

    it 'creates message in given conversation' do
      expect { operation }.to change { Message.count }
        .from(0).to(1)
      expect(operation['model'].body).to eq(message_params[:body])
    end
  end

  context 'empty hash provided' do
    let(:operation) do
      Message::Operation::Create.call({})
    end

    it 'must be failure' do
      expect(operation).to be_failure
    end
  end

  context 'current_user provided' do
    let(:operation) do
      Message::Operation::Create.call({}, 'current_user' => sender)
    end

    it 'requires conversation_id' do
      expect(operation).to be_failure
    end
  end

  context 'body missing' do
    let(:operation) do
      Message::Operation::Create.call(message_params.except(:body),
                                      'current_user' => sender)
    end

    it 'must validate params' do
      expect(operation['contract.default'].errors.messages)
        .to include(:body)
    end
  end

  def message_params
    {
      body: 'Hey, what\'s up',
      conversation_id: conversation.id
    }
  end
end
