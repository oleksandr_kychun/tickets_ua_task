class UsersController < ApplicationController
  def edit
    run User::Operation::Update::Present, params,
        'current_user' => tyrant.current_user do
      return render cell(User::Cell::UpdateForm, @form)
    end
    redirect_to root_path
  end

  def update
    run User::Operation::Update, params[:user],
        'current_user' => tyrant.current_user do
      flash[:notice] = 'Profile updated!'
      return redirect_to root_path
    end
    render cell(User::Cell::UpdateForm, @form)
  end

  def new
    run User::Operation::Create::Present, params,
        'current_user' => tyrant.current_user do
      return render cell(User::Cell::CreateForm, @form)
    end
    redirect_to root_path
  end

  def create
    run User::Operation::Create, params[:user],
        'current_user' => tyrant.current_user do
      flash[:notice] = 'User created!'
      return redirect_to root_path
    end
    render cell(User::Cell::CreateForm, @form)
  end

  def index
    conversation_op = run Conversation::Operation::UnreadData, {},
                          'current_user' => tyrant.current_user  
    run User::Operation::Index, params,
        'current_user' => tyrant.current_user do |result|
      return render cell(User::Cell::Index, result['model'],
                         'current_user' => tyrant.current_user,
                         'conversation_op' => conversation_op)
    end
    redirect_to sessions_sign_in_form_path
  end

  def destroy
    run User::Operation::Delete, params,
        'current_user' => tyrant.current_user
    redirect_to users_path
  end
end
