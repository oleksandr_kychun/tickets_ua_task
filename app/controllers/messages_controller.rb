class MessagesController < ApplicationController
  def create
    run Message::Operation::Create, params[:message],
        'current_user' => tyrant.current_user do |result|
      return redirect_to conversation_path(result['model'].conversation_id)
    end
    redirect_to users_path
  end
end
