class ConversationsController < ApplicationController
  def create
    current_user = { 'current_user' => tyrant.current_user }
    run Conversation::Operation::Create, params, current_user do |result|
      return render cell(Conversation::Cell::Show, result['model'],
                         message_op: message_op(result['model'].id))
    end
    redirect_to users_path
  end

  def show
    run Conversation::Operation::Show, params,
        'current_user' => tyrant.current_user do |result|
      return render cell(Conversation::Cell::Show,
                         result['model'],
                         current_user_id: tyrant.current_user.id,
                         message_op: message_op(params['id']))
    end
  end

  def index
    run Conversation::Operation::Index, params,
        'current_user' => tyrant.current_user do |result|
      return render cell(Conversation::Cell::Index, result['model'],
                         'current_user' => result['current_user'])
    end
    redirect_to root_path
  end

  private

  def message_op(conversation_id)
    run Message::Operation::Create::Present,
        { conversation_id: conversation_id },
        'current_user' => tyrant.current_user
  end
end
