class MessagePolicy
  def initialize(user, model)
    @user = user || NoUser.new
    @model = model
  end

  def delete?
    user.id == model.user_id
  end

  private

  attr_reader :user, :model
end