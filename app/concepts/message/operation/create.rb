class Message::Operation
  # create operation
  class Create < Trailblazer::Operation
    # new operation
    class Present < Trailblazer::Operation
      step :assign_model_with_conversation
      step Policy::Pundit(ConversationPolicy, :available?)
      step Model(Message, :new)
      step Contract::Build(constant: Message::Contract::Create)

      private

      def assign_model_with_conversation(operation, params:, **)
        model = Conversation.find_by(id: params[:conversation_id])
        operation['model'] = model
        operation['conversation_id'] = model && model.id
      end
    end

    step Nested(Present)
    step Contract::Validate()
    step :assign_user_id
    step Contract::Persist()

    private

    def assign_user_id(_operation, model:, current_user:, **)
      model.user_id = current_user.id
    end
  end
end
