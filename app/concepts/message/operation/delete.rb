class Message::Operation
  # delete operation
  class Delete < Trailblazer::Operation
    step Model(Message, :find_by)
    step Policy::Pundit(MessagePolicy, :delete?)
    step :destroy_model!

    private

    def destroy_model!(_operation, model:, **)
      model.destroy
    end
  end
end
