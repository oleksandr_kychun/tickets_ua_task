module Message::Contract
  # message contract
  class Create < Reform::Form
    property :body
    property :conversation_id

    validates :body, :conversation_id, presence: true
    validates :body, length: { maximum: 245 }
  end
end
