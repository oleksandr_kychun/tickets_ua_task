module Message::Cell
  class Index < Trailblazer::Cell
    private

    def collection
      model.messages.old_to_recent
    end

    def current_user_id
      options['current_user_id']
    end
  end
end
