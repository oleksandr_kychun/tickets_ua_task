module Message::Cell
  class Show < Trailblazer::Cell
    OWN_MESSAGE_CLASSES = 'alert alert-success col-md-offset-6 col-md-6'.freeze
    RESEIVING_MESSAGE_CLASSES = 'alert alert-info col-md-6'.freeze

    property :body
    property :status

    private

    def current_user_id
      options['current_user_id']
    end

    def created_at
      model.created_at.strftime("at %I:%M%p")
    end

    def message_class
      return OWN_MESSAGE_CLASSES if own_message?
      RESEIVING_MESSAGE_CLASSES
    end

    def own_message?
      model.user_id == current_user_id
    end

    def own_message_timestamp
      return if own_message?
      created_at
    end

    def received_message_timestamp
      return unless own_message?
      created_at
    end
  end
end
