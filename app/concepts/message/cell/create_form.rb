module Message::Cell
  class CreateForm < Trailblazer::Cell
    include ActionView::RecordIdentifier
    include ActionView::Helpers::FormOptionsHelper
    include SimpleForm::ActionViewExtensions::FormHelper

    private

    def conversation_id
      options['conversation_id']
    end
  end
end
