# conversation policy rules
class ConversationPolicy
  def initialize(user, model)
    @user = user || NoUser.new
    @model = model
  end

  def create?
    user.user?
  end

  def available?
    return false unless user.user?
    return false unless model
    model.user_ids.include?(user.id)
  end

  private

  attr_reader :user, :model
end
