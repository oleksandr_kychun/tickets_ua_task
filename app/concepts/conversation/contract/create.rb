module Conversation::Contract
  # conversation create contract
  class Create < Reform::Form
    property :sender_id, virtual: true
    property :receiver_id, virtual: true

    validates :sender_id, :receiver_id, presence: true
    validate :attendees_uniqueness

    private

    def attendees_uniqueness
      return if sender_id != receiver_id
      errors.add(:receiver_id,
                 'You can\'t have conversaion with yourself')
    end
  end
end
