module Conversation::Cell
  class UnreadData < Trailblazer::Cell
    private

    def current_user
      options[:current_user]
    end

    def unread_any?
      model && model.any?
    end

    def clickable_result
      link_to formatted_data, conversations_path
    end

    def formatted_data
      model.map { |conversation|
        format_conversation(conversation)
      }.join("\n")
    end

    def format_conversation(conversation)
      receiver_id = (conversation.user_ids - [current_user.id]).first
      receiver = current_user.class.find_by(id: receiver_id)
      "#{receiver.email} #{conversation.messages.unread(current_user.id).size}"\
        ' unread messages'
    end
  end
end
