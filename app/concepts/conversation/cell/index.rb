module Conversation::Cell
  class Index < Trailblazer::Cell
    def current_user
      options['current_user']
    end

    def email_of_receiver(conversation)
      receiver_id = (conversation.user_ids - [current_user.id]).first
      receiver = current_user.class.find_by(id: receiver_id)
      receiver && receiver.email
    end
  end
end
