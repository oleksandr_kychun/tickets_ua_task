module Conversation::Cell
  class Show < Trailblazer::Cell
    private

    def current_user_id
      options[:current_user_id]
    end

    def message_op
      options[:message_op]
    end

    def message_model
      message_op['model']
    end

    def conversation_id
      message_op['conversation_id']
    end
  end
end
