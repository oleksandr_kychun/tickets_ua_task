class Conversation::Operation
  # present conversation
  class Show < Trailblazer::Operation
    READ = 1
    step Model(Conversation, :find_by)
    step Policy::Pundit(ConversationPolicy, :available?)
    step :make_messages_visible!

    private

    def make_messages_visible!(_operation, model:, current_user:, **)
      model.messages.unread(current_user.id).update_all(status: READ)
    end
  end
end
