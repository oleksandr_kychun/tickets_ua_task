class Conversation::Operation
  class Index < Trailblazer::Operation
    step Policy::Pundit(ConversationPolicy, :create?)
    step :model!

    private

    def model!(operation, current_user:, **)
      operation['model'] = current_user.conversations
                                       .unread_data(current_user.id)
    end
  end
end