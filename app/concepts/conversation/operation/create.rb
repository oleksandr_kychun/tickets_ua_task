class Conversation::Operation
  # conversation creation
  class Create < Trailblazer::Operation
    step Policy::Pundit(ConversationPolicy, :create?)
    step Model(Conversation, :new)
    step Contract::Build(constant: Conversation::Contract::Create)
    step :assign_sender_id
    step Contract::Validate()
    step :create_joining_records_with_attendies
    step Contract::Persist()

    private

    def assign_sender_id(operation, current_user: nil, **)
      return true unless current_user
      operation['contract.default'].sender_id = current_user.id
    end

    def create_joining_records_with_attendies(operation, model:, **)
      contract = operation['contract.default']
      model.user_ids = [contract.sender_id, contract.receiver_id]
    end
  end
end
