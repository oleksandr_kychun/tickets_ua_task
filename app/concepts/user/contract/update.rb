#:contract
module User::Contract
  # contract for user creation
  class Update < Reform::Form
    property :first_name
    property :last_name
    property :password, virtual: true
    property :confirm_password, virtual: true
    property :current_password, virtual: true

    validates :current_password, presence: true
    validates :password, :confirm_password,
              presence: true, if: :password_given?
    validate :correct_password?
    validate :password_ok?, if: :password_ok?

    private

    def password_given?
      !password.blank?
    end

    def correct_password?
      return unless current_password
      return if Tyrant::Authenticatable.new(model).digest?(current_password)
      errors.add(:current_password, 'Wrong existing password')
    end

    def password_ok?
      return unless password
      return if password == confirm_password
      errors.add(:password, 'Passwords don\'t match')
    end
  end
end
