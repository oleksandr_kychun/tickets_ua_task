module User::Contract
  # new user form object
  class Create < Reform::Form
    property :first_name
    property :last_name
    property :password, virtual: true
    property :email

    validates :password, :email, presence: true
    validates_uniqueness_of :email
  end
end
