class User::Operation
  # user deletion
  class Delete < Trailblazer::Operation
    step Model(User, :find_by)
    step Policy::Pundit(UserPolicy, :destroy?)
    step :destroy!

    private

    def destroy!(_operation, model:, **)
      model.destroy
    end
  end
end
