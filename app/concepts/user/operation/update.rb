class User::Operation
  # user update operation
  class Update < Trailblazer::Operation
    # user update form
    class Present < Trailblazer::Operation
      step Model(User, :find_by)
      step Contract::Build(constant: User::Contract::Update)
      step Policy::Pundit(UserPolicy, :update?)
    end

    step Nested(Present)
    step Contract::Validate()
    step :invoke_tyrant
    step Contract::Persist()

    private

    def invoke_tyrant(options, _hsh)
      contract = options['contract.default']
      auth = Tyrant::Authenticatable.new(contract.model)
      auth.digest!(contract.password)
      auth.confirmed!
      auth.sync
    end
  end
end
