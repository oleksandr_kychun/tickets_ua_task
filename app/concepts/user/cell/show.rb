module User::Cell
  class Show < Trailblazer::Cell
    property :email
    property :first_name
    property :last_name

    private

    def conversation_link
      if conversation_with_given_user
        link_to link_header,
                conversation_path(conversation_with_given_user)
      else
        link_to link_header,
                conversations_path(receiver_id: model.id), method: :post
      end
    end

    def edit_link
      link_to 'edit', edit_user_path(model) if current_user.admin?
    end

    def delete_link
      link_to 'delete', user_path(model), method: :delete if current_user.admin?
    end

    def conversation_with_given_user
      options[:current_user].conversations.with_other_user(model.id).first
    end

    def link_header
      "conversation with #{model.email}"
    end

    def current_user
      options[:current_user]
    end
  end
end
