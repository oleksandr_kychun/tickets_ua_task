module User::Cell
  class Index < Trailblazer::Cell
    def current_user
      options['current_user']
    end

    def unread_data
      options['conversation_op']['model']
    end
  end
end
