class Session::Operation
  # sign_up service
  class SignUp < Trailblazer::Operation
    # sign up form
    class Present < Trailblazer::Operation
      step Model(User, :new)
      step Contract::Build(constant: Session::Contract::Create)
    end

    step Nested(Present)
    step :set_default_role
    step Contract::Validate()
    step :invoke_tyrant
    step Contract::Persist()

    private

    def set_default_role(_options, hsh)
      hsh[:model].role = 0
    end

    def invoke_tyrant(options, _hsh)
      contract = options['contract.default']
      auth = Tyrant::Authenticatable.new(contract.model)
      auth.digest!(contract.password)
      auth.confirmed!
      auth.sync
    end
  end
end
