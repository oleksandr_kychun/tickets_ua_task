class NoUser
  def method_missing(_method_name, *_args, &_block)
    false
  end

  def respond_to_missing?(_method_name, _include_private = false)
    true
  end
end
