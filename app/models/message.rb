class Message < ApplicationRecord
  belongs_to :user, required: false
  belongs_to :conversation, required: false
  enum status: %i[unread seen]
  scope :old_to_recent, ->{ order(created_at: :asc) }
  scope :unread, ->(user_id) { where(status: 0).where.not(user_id: user_id) }
end
