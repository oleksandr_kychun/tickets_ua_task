class Conversation < ApplicationRecord
  UNREAD = 0
  has_and_belongs_to_many :users
  has_many :messages, dependent: :destroy

  scope :with_other_user, ->(id) { joins(:users).where('users.id = ?', id) }
  scope :unread_data,
        ->(user_id) {
          joins(:messages).includes(:messages)
                          .where(messages: { status: UNREAD })
                          .where.not(messages: { user_id: user_id })
        }
end
