class User < ApplicationRecord
  enum role: %i[user vip admin]
  serialize :auth_meta_data
  has_and_belongs_to_many :conversations

  scope :excluding_user, ->(id) { where.not(id: id) }
end
