class CreateConversationsUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :conversations_users, id: false do |t|
      t.belongs_to :conversation, foreign_key: true
      t.belongs_to :user, foreign_key: true
    end
  end
end
