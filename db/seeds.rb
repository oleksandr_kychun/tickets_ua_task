ADMIN_PASS = '12345678'.freeze
admin = User.new(email: 'admin@gmail.com', role: 2)
auth = Tyrant::Authenticatable.new(admin)
auth.digest!(ADMIN_PASS)
auth.confirmed!
auth.sync
admin.save!

ordinary_user1 = User.new(email: 'valid1@gmail.com')
auth = Tyrant::Authenticatable.new(ordinary_user1)
auth.digest!(ADMIN_PASS)
auth.confirmed!
auth.sync
ordinary_user1.save!

ordinary_user2 = User.new(email: 'valid2@gmail.com')
auth = Tyrant::Authenticatable.new(ordinary_user2)
auth.digest!(ADMIN_PASS)
auth.confirmed!
auth.sync
ordinary_user2.save!
